window.fbAsyncInit = function() {
    FB.init({
        appId: '104647926536525',
        xfbml: true,
        version: 'v2.3'
    });
};

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

var btn = document.getElementById('fb-share');
btn.addEventListener('click', fbShare);

var twBtn = document.getElementById('tw-share');
twBtn.addEventListener('click', twShare);


window.twttr = (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0],
        t = window.twttr || {};
    if (d.getElementById(id)) return t;
    js = d.createElement(s);
    js.id = id;
    js.src = "https://platform.twitter.com/widgets.js";
    fjs.parentNode.insertBefore(js, fjs);

    t._e = [];
    t.ready = function(f) {
        t._e.push(f);
    };

    return t;
}(document, "script", "twitter-wjs"));

twttr.widgets.load(
    document.getElementById("container");
);

function fbShare() {
    var currentUrl = document.location.href;

    console.log('FB URL to share:', currentUrl);

    var description = 'Description Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi eaque labore, architecto cumque illo aspernatur quis corrupti odio, voluptate quia adipisci omnis? Eaque ipsa necessitatibus ad quae, molestiae dicta est.';

    var caption = 'This is caption';

    //console.log(description[index], caption);

    FB.ui({
        method: 'feed',
        //href: currentUrl, //share from current URL of the app
        link: currentUrl,
        caption: caption,
        name: 'This is name',
        picture: 'http://www.harekrsna.de/lotus-feet-hand/beautiful_pink_lotus.jpg',
        description: description
    }, function(response) {
        if (response && response.post_id) {
            console.log('Post was published.');
        } else {
            console.log('Post was not published.');
        }
    });
}

function twShare(e) {
    e.preventDefault();

    var link = e.currentTarget.getAttribute('href');
    var ref = e.currentTarget.getAttribute('ref') || 'facebook';
    var width = 575,
        height = 400,
        left = window.innerWidth / 2,
        top = window.innerHeight / 2,
        url = link,
        opts = 'status=1' +
        ',width=' + width +
        ',height=' + height +
        ',top=' + top +
        ',left=' + left;
    window.open(url, 'ref' + Math.random(), opts);
}

function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();
  console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
  console.log('Name: ' + profile.getName());
  console.log('Image URL: ' + profile.getImageUrl());
  console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
}
